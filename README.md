te31_scraper
==========
http://te31.com scraper

te31.com 호게에 접속하여 게시물 목록을 얻어온 뒤, 이미지 첨부 게시글만을 하나씩 읽어 DB에 넣는 스크랩퍼

# 저작권
The Artistic License 2.0

LICENSE 파일 참조.

# 요구사항
* Perl 5.10+
 * DBI
  * DBD::mysql
 * HTML::TreeBuilder::LibXML
 * Web::Scraper
 * LWP::UserAgent
* 기타 등등...
* MySQL or MariaDB

# 사용 방법
1. SQL 테이블 생성
1. ./make_dir.pl 실행(이미지와 썸네일 00 ~ ff 로 분류 저장용 디렉토리 생성.)
1. rgr_db.pl 파일의 Shebang 확인(기본값은 #!/usr/bin/env perl ), 실행권한 주기(chmod u+x rgr_db.pl)
1. ./rgr_db.pl
 1. 주기적인 실행을 원하는 경우 loop.sh 스크립트 실행. 6~11초 간격으로 실행됨.


# 주의사항
1. './rgr_db.pl 1 300' 과 같은 방식으로 실행하면 300번째 페이지에서 1페이지(가장 최근 페이지)까지의 글을 가져오게 됨. 이미 저장된 글은 다시 읽지 않음.

# 테이블 구조
```
#!sql
describe md5_list;
+----------+----------+------+-----+---------+----------------+
| Field    | Type     | Null | Key | Default | Extra          |
+----------+----------+------+-----+---------+----------------+
| id       | int(11)  | NO   | PRI | NULL    | auto_increment |
| saveTime | datetime | NO   |     | NULL    |                |
| name     | char(37) | NO   |     | NULL    |                |
+----------+----------+------+-----+---------+----------------+

describe rgrong;
+-------+----------+------+-----+---------+----------------+
| Field | Type     | Null | Key | Default | Extra          |
+-------+----------+------+-----+---------+----------------+
| id    | int(11)  | NO   | PRI | NULL    | auto_increment |
| no    | int(11)  | NO   |     | NULL    |                |
| name  | char(37) | NO   |     | NULL    |                |
+-------+----------+------+-----+---------+----------------+
```
